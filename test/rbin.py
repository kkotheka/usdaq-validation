# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 10:14:08 2021

@author: Shyam
"""
import codecs

data = []
nLines = 65535
n = 0
with open('dump.bin', 'rb') as f:
    for chunk in iter(lambda: f.read(4), b''):
        if(n < nLines):
            string = codecs.encode(chunk, 'hex').decode("utf-8")
            data.append(string)
            n+=1


for i in range(len(data)):
    line = data[i]

    line = [line[2*j:2*(j+1)] for j in range(4)]
    line = [k[::-1] for k in line]

    data[i] = "0x" + "".join(line)[::-1]


out = "dump_65535.txt"
out_file = open(out, "w")
[out_file.write(c + "\n") for c in data]
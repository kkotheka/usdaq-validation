"""
Created on Fri May 16 2021

@author: Kunal Kothekar

This program is used to analyze the ADC values coming out of vanilla felix firmware

"""
#! /usr/bin/env python
import sys
import matplotlib.pyplot as plt
import numpy as np

#class to read 33b file and separete the 33b and flags
class Wib_Split:
    def _init_(self, wib_value=0, flag=0):
        self.wib_value = wib_value
        self.flag = flag



    def from_line(self, line):
        values = line.split(' ')
        self.wib_value = int(values[0], 16)
        self.flag = int(values[1])

    def __str__(self):
        return "\n".join(
            [
                "0x{0:08x} {1}".format(d, f) for d, f in zip(self.wib_value, self.flag)
            ]
        )


#open the input file and separate wib value and flags
ip_file = sys.argv[1] 
with open(ip_file) as f:
    wib_values = []
    for line in f:
        wib_values.append(Wib_Split())
        wib_values[-1].from_line(line)

#open the output file and read the values

op_file = sys.argv[2]
with open(op_file) as f1:
    adc_values = []
    for line in f1:
        adc_values.append(int(line,16))


op_file1 = sys.argv[3]
with open(op_file1) as f1:
    adc_values1 = []
    for line in f1:
        adc_values1.append(int(line,16))

#module for plotting
ip_values = []
ip_count = 0
for i in range(len(wib_values)):
    if(wib_values[i].flag != 1):
        ip_count = ip_count +1
        ip_values.append(wib_values[i].wib_value)

op_values =[]
op_count = 0
for i in range(len(adc_values)):
    if(adc_values[i] != 0):
        op_count = op_count+1
        op_values.append(adc_values[i])
print("input count = ", ip_count)
print("output count = ", op_count)

op_values1 =[]
op_count1 = 0
for i in range(len(adc_values1)):
    if(adc_values1[i] != 0):
        op_count1 = op_count1+1
        op_values1.append(adc_values1[i])
print("input count = ", ip_count)
print("output count file 2= ", op_count1)


        
#plt.plot(ip_values, op_values, 'r--')

#plt.show()


#plots
#bins_ip = np.arange(min(ip_values), max(ip_values) + 1000, 1000)
#bins_op = np.arange(min(op_values), max(ip_values) + 1000, 1000)

plt.hist(ip_values,histtype='step', color = 'blue',label='ip')
plt.hist(op_values,histtype= 'step',color = 'red',label='tdaq_op')
plt.hist(op_values1,histtype= 'step',color = 'green',label='vanilla_op')

plt.legend(prop={'size': 10})
plt.grid(axis='y', alpha=0.75)
plt.xlabel('32b values',fontsize=12)
plt.ylabel('#count',fontsize=10)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('Distribution of 32b values in input and output',fontsize=15)

plt.savefig("FixedHits_A_dc_single_1fiber.pdf")
plt.close()




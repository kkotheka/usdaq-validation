"""
Created on June 2021

@author: Kunal Kothekar

This program is used to add the CRC checksum to the 33b patterns

update:This program is edited to write emuconfig files on 05th July 2021

Input : wib_33b.txt (without the checksum)

Output : emuconfig

Also needs a crc.py module to calculate the checksum.
"""
import sys # For sys.argv and sys.exit
import crc
from itertools import dropwhile

#class to read 33b file and separete the 33b and flags
class Wib_Split:
    def _init_(self, wib_value=0, flag=0):
        self.wib_value = wib_value
        self.flag = flag



    def from_line(self, line):
        values = line.split(' ')
        self.wib_value = int(values[0], 16)
        self.flag = int(values[1])

    def __str__(self):
        return "\n".join(
            [
                "0x{0:08x} {1}".format(d, f) for d, f in zip(self.wib_value, self.flag)
            ]
        )



#open the files and separate wib value and flags
txt_file = sys.argv[1] 
with open(txt_file) as f:
    wib_values = []
    for line in f:
        wib_values.append(Wib_Split())
        wib_values[-1].from_line(line)

# to test if the values are stored correctly        
#print(hex(wib_values[0].wib_value), hex(wib_values[0].flag))



#pass the values and calculate CRC from crc.py
index = 0
count = 0
eof = 0x1000000dc
for i in range(len(wib_values)):
    
        
    if(wib_values[i].flag == 0):
        count = count +1
        
    if((wib_values[i].wib_value == 0x000000dc) and (wib_values[i].flag == 1)):
        print(index, index+count)
        checksum = crc.CRC(wib_values[index:index+count])
        checksum = int(hex((checksum << 8) | eof),16) #bitshift the checksum to accomodate eof characters
        wib_values[i].wib_value = checksum  #add the checksum in the place of 0x000000dc
        index =i
        count =0
        #print(index, index+count)
        #print(hex(checksum))

    if(wib_values[i].flag == 1):
        index = index +1


##For testing purposes
#add value to the wib list
#wib_values[116].wib_value = int('0x0b7165dc',16)
#wib_values[116].flag = int('0')


#for i in range(len(wib_values)):

 #   print(hex(wib_values[i].wib_value), hex(wib_values[i].flag))
 

#write the file in 33b format including eof and CRC

# with open('FixedADC_A_wib_33b_CRC_dc.txt', 'w') as f1:
#     for i in range(len(wib_values)):
#         f1.writelines(["0x{0:08x} {1}\n".format(wib_values[i].wib_value, wib_values[i].flag)])
#         #contents = "\n".join(["0x{0:08x} {1}".format(wib_values[i].wib_value, wib_values[i].flag)])

#     #f1.writelines(contents)
#     f1.close()

#write the file in emulator configuration format
#Needs to be 8192 lines long, after the pattern end, 0x000000bc idle charcters are added.

with open('UniqueHits_E_emuconfig', 'w') as f1:
    if(len(wib_values) < 8192):
        for i in range(len(wib_values)):
            if(wib_values[i].wib_value == 0x0000003c):
                 f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i))])
                 f1.writelines(["FE_EMU_CONFIG_WRDATA=0x10000003c\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=0\n"])
            elif(wib_values[i].wib_value == 0x000000bc):
                 f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i))])
                 f1.writelines(["FE_EMU_CONFIG_WRDATA=0x1000000bc\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=0\n"])
            else:
                 f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i))])
                 f1.writelines(["FE_EMU_CONFIG_WRDATA=0x{0:09x}\n".format(wib_values[i].wib_value)])
                 f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=0\n"])
        for i in range(8192 - len(wib_values)):
            f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i+len(wib_values)))])
            f1.writelines(["FE_EMU_CONFIG_WRDATA=0x1000000bc\n"])
            f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
            f1.writelines(["FE_EMU_CONFIG_WE=0\n"])

    if(len(wib_values) > 8192):
        for i in range(len(wib_values)):
            if(wib_values[i].wib_value == 0x0000003c):
                 f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i))])
                 f1.writelines(["FE_EMU_CONFIG_WRDATA=0x10000003c\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=0\n"])
            elif(wib_values[i].wib_value == 0x000000bc):
                 f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i))])
                 f1.writelines(["FE_EMU_CONFIG_WRDATA=0x1000000bc\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=0\n"])
            else:
                 f1.writelines(["FE_EMU_CONFIG_WRADDR={}\n".format(hex(i))])
                 f1.writelines(["FE_EMU_CONFIG_WRDATA=0x{0:09x}\n".format(wib_values[i].wib_value)])
                 f1.writelines(["FE_EMU_CONFIG_WE=1\n"])
                 f1.writelines(["FE_EMU_CONFIG_WE=0\n"])
            
       

    #f1.writelines(contents)
    f1.close()

